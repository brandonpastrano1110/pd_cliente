package programaciondistribuida;

import java.net.*;
import java.io.*;

public class dlgCliente extends javax.swing.JDialog {

    private PrintStream salida = null;
    private Socket ClienteSocket = null; // Socket
    private String IP = "192.168.1.13"; //Ip, domino, o Nombre de la maquina
    private final int PUERTO = 65001;

    public dlgCliente() { 
        initComponents();
        try {
            ClienteSocket = new Socket(IP, PUERTO);
            salida = new PrintStream(ClienteSocket.getOutputStream());
        } catch (UnknownHostException e) {
            System.err.println(IP + " desconocido");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("No se puede establecer conexión");
            System.exit(1);
        }
        salida.println("\t");
        salida.println("iniciando");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        EnviarServidro = new javax.swing.JButton();
        Salir = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        rbnCirculo = new javax.swing.JRadioButton();
        rbnRec = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        rbnAzul = new javax.swing.JRadioButton();
        rbnVerde = new javax.swing.JRadioButton();
        rbnRojo = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setText("  CLIENTE");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 30, 60, 20);

        EnviarServidro.setText("Enviar al servidor");
        EnviarServidro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EnviarServidroActionPerformed(evt);
            }
        });
        getContentPane().add(EnviarServidro);
        EnviarServidro.setBounds(40, 330, 140, 25);

        Salir.setText("Salir");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });
        getContentPane().add(Salir);
        Salir.setBounds(200, 330, 90, 25);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 153, 0)));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        jLabel4.setBackground(new java.awt.Color(153, 255, 255));
        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 255, 255));
        jLabel4.setText(" ~~~~~ FIGURA ~~~~~ ");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(20, 20, 220, 25);

        buttonGroup1.add(rbnCirculo);
        rbnCirculo.setText("Circulo");
        jPanel1.add(rbnCirculo);
        rbnCirculo.setBounds(40, 60, 80, 25);

        buttonGroup1.add(rbnRec);
        rbnRec.setText("Rectangulo");
        jPanel1.add(rbnRec);
        rbnRec.setBounds(150, 60, 110, 25);

        jLabel3.setBackground(new java.awt.Color(0, 102, 153));
        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(204, 255, 255));
        jLabel3.setText("~~~~~ COLORES ~~~~~ ");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 100, 230, 25);

        buttonGroup2.add(rbnAzul);
        rbnAzul.setText("Azul");
        rbnAzul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbnAzulActionPerformed(evt);
            }
        });
        jPanel1.add(rbnAzul);
        rbnAzul.setBounds(20, 140, 60, 25);

        buttonGroup2.add(rbnVerde);
        rbnVerde.setText("Verde");
        jPanel1.add(rbnVerde);
        rbnVerde.setBounds(110, 140, 70, 25);

        buttonGroup2.add(rbnRojo);
        rbnRojo.setText("Rojo");
        jPanel1.add(rbnRojo);
        rbnRojo.setBounds(190, 140, 60, 25);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(20, 80, 300, 200);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private String SelectColor(){
        String color="";
        if(rbnCirculo.isSelected()){
             if(rbnAzul.isSelected()){
                 color="11";
             }
             else if(rbnVerde.isSelected()){
                 color="12";
             }
             else if(rbnRojo.isSelected()){
                 color="13";
             }
             else
                 System.err.println("Selecciona color");
        }
        else if(rbnRec.isSelected()){
             if(rbnAzul.isSelected()){
                 color="21";
             }
             else if(rbnVerde.isSelected()){
                 color="22";
             }
             else if(rbnRojo.isSelected()){
                 color="23";
             }
             else
                 System.err.println("Selecciona color");
        }
        else{
            System.err.println("Selecciona figura");
        }
        return color;
    }
    private void EnviarServidroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EnviarServidroActionPerformed
        salida.println(SelectColor());
        //salida.println(txtMensaje.getText()); // ENVIAR MENSAJE
       // txtMensaje.setText("");
    }//GEN-LAST:event_EnviarServidroActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        // TODO add your handling code here:
        salida.println("Exit");
        try {
            ClienteSocket.close();
        } catch (IOException e) {
            System.err.println("No se puede establecer conexión");
        }
    }//GEN-LAST:event_SalirActionPerformed

    private void rbnAzulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbnAzulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbnAzulActionPerformed
 

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {} 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton EnviarServidro;
    private javax.swing.JButton Salir;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton rbnAzul;
    private javax.swing.JRadioButton rbnCirculo;
    private javax.swing.JRadioButton rbnRec;
    private javax.swing.JRadioButton rbnRojo;
    private javax.swing.JRadioButton rbnVerde;
    // End of variables declaration//GEN-END:variables
}
